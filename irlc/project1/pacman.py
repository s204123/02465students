# This file may not be shared/redistributed without permission. Please read copyright notice in the git repo. If this file contains other copyright notices disregard this text.
from collections import defaultdict
from fractions import Fraction
from irlc import train
from irlc.ex02.dp_model import DPModel
#from irlc.ex02.dp import DP_stochastic
from irlc.ex02.dp_agent import DynamicalProgrammingAgent
from irlc.pacman.pacman_environment import GymPacmanEnvironment
import numpy as np
from irlc import VideoMonitor
from irlc.ex02.graph_traversal import policy_rollout
#!s=east
east = """ 
%%%%%%%%
% P   .%
%%%%%%%% """ 

east2 = """
%%%%%%%%
%    P.%
%%%%%%%% """

SS2tiny = """
%%%%%%
%.P  %
% GG.%
%%%%%%
"""

SS0tiny = """
%%%%%%
%.P  %
%   .%
%%%%%%
"""

SS1tiny = """
%%%%%%
%.P  %
%  G.%
%%%%%%
"""

datadiscs = """
%%%%%%%
%    .%
%.P%% %
%.   .%
%%%%%%%
"""

# TODO: 29 lines missing.
# raise NotImplementedError("Put your own code here")

class pacman_dp(DPModel):
    def __init__(self, x, N):
        super().__init__(N=N)
        self.states = get_future_states(x, N)
        self.x = x
    
    def f(self, x, u, w, k):
        if x.isWin() or x.isLose():
            self.x = x
            return x
        x = x.f(u)
        if x.isWin() or x.isLose():
            self.x = x
            return x
        for _ in range(x.players()-1):
            x = x.f(np.random.choice(x.A()))
            if x.isLose():
                break
        self.x = x
        return x
    
    
    def g(self, x, u, w, k):
        return -x.data.scoreChange

    def gN(self, x):
        if x.isWin():
            return -1
        else:
            return 0
    
    def A(self, x, k):
        return x.A()
    
    def S(self, k):
        #return get_future_states(self.x, 1)[1]
        return self.states[k] 

    def Pw(self, x, u, k):
        return p_next(x, u)

class pacman_dp2(pacman_dp):

    def __init__(self, x, N):
        super().__init__(x, N)
    
    def g(self, x, u, w, k):
        return 0   


def get_states(X):
    if type(X) == list or type(X) == set:
        dx = []
        for s in X:
            if not (s.isWin() or s.isLose()):
                for a in s.A():
                    dx.append(s.f(a))
            else:
                dx.append(s)
    else:
        dx = []
        if not (X.isWin() or X.isLose()):
            for a in X.A():
                dx.append(X.f(a))
        else:
            dx.append(X)
    return dx

def get_future_states(x, N): 
    # TODO: 4 lines missing.
    X = [[x]]
    np = x.players()
    for _ in range(N):
        for __ in range(np):
            x = set(get_states(x))
        X.append(list(x))
    return X

def p_next(x, u): 
    """ Given the agent is in GameState x and takes action u, the game will transition to a new state xp.
    The state xp will be random when there are ghosts. This function should return a dictionary of the form

    {..., xp: p, ...}

    of all possible next states xp and their probability -- you need to compute this probability.

    Hints:
        * In the above, xp should be a GameState, and p will be a float. These are generated using the functions in the GameState x.
        * Start simple (zero ghosts). Then make it work with one ghosts, and then finally with any number of ghosts.
        * Remember the ghosts move at random. I.e. if a ghost has 3 available actions, it will choose one with probability 1/3
        * The slightly tricky part is that when there are multiple ghosts, different actions by the individual ghosts may lead to the same final state
        * Check the probabilities sum to 1. This will be your main way of debugging your code and catching issues relating to the previous point.
    """
    # TODO: 8 lines missing. 


    if not (x.isWin() or x.isLose()):
        x = x.f(u) 
        if not (x.isWin() or x.isLose()):
            for _ in range(x.players()-1): # implicit -1
                x = (get_states(x))

    states = defaultdict(lambda: 0)
    if type(x) == list or type(x) == set:
        N = len(x)
        for s in x:
            states[s] += (np.float64(1/N)) #Fraction(1,N)
    else:
        states[x] = 1
    assert np.round(sum(list(states.values())),6) == 1, "Something went wrong"
    return dict(states)


def go_east(map): 
    """ Given a map-string map (see examples in the top of this file) that can be solved by only going east, this will return
    a list of states Pacman will traverse. The list it returns should therefore be of the form:

    [s0, s1, s2, ..., sn]

    where each sk is a GameState object, the first element s0 is the start-configuration (corresponding to that in the Map),
    and the last configuration sn is a won GameState obtained by going east.

    Note this function should work independently of the number of required east-actions.

    Hints:
        * Use the GymPacmanEnvironment class. The report description will contain information about how to set it up, as will pacman_demo.py
        * Use this environment to get the first GameState, then use the recommended functions to go east
    """
    # TODO: 4 lines missing.
    # Smarter way would be to get tiles from r2d2 to the east wall and loop over those.
    env = GymPacmanEnvironment(layout_str=map, animate_movement=True)
    x   = env.reset()
    S   = []
    done = False
    while not done:
        A = x.getLegalActions()
        # A = x.A()
        if 'East' in A:
            S.append(x) 
            x, _r, done, _ = env.step('East')
            # x, _r, done, _ = x.f('East')
    S.append(x)
    return S
    #raise NotImplementedError("Return the list of states pacman will traverse if he goes east until he wins the map")


    
def win_probability(fmap, N=10): 
    """ Assuming you get a reward of -1 on wining (and otherwise zero), the win probability is -J_pi(x_0). """
    # TODO: 4 lines missing.
    env = GymPacmanEnvironment(layout_str=fmap)
    #env = VideoMonitor(env)
    x = env.reset()
    #S = get_future_states(x,N)
    #O = map(lambda x: sum([s.isWin() for s in x]),S)
    #O = list(O)
    #res = O[-1]/len(S[-1])
    model = pacman_dp2(x, N)
    J, pi = DP_stochastic(model)
    res = (-J[0][x])
    print(res)
    return res

def shortest_path(map, N=10): 
    """ If each move has a cost of 1, the shortest path is the path with the lowest cost.
    The actions should be the list of actions taken.
    The states should be a list of states the agent visit. The first should be the initial state and the last
    should be the won state. """
    # TODO: 4 lines missing.

    x = GymPacmanEnvironment(layout_str=map).reset()
    model = pacman_dp(x, N)
    J, pi = DP_stochastic(model)
    actions = []
    states = [x]
    for k in range(N):
        a = pi[k][x]
        x = x.f(a)
        actions.append(a)
        states.append(x)
        if x.isWin(): break
    return actions, states
    #raise NotImplementedError("Return the cost of the shortest path, the list of actions taken, and the list of states.")


def no_ghosts():
    # Check the pacman_demo.py file for help on the GameState class and how to get started.
    # This function contains examples of calling your functions. However, you should use unitgrade to verify correctness.

    ## Problem 1: Lets try to go East. Run this code to see if the states you return looks sensible.
    states = go_east(east)
    for s in states:
        print(str(s))

    ## Problem 3: try the p_next function for a few empty environments. Does the result look sensible?
    
    x = GymPacmanEnvironment(layout_str=east).reset()
    action = x.A()[0]
    print(f"Transitions when taking action {action} in map: 'east'")
    print(x)
    print(p_next(x, action))  # use str(state) to get a nicer representation.

    print(f"Transitions when taking action {action} in map: 'east2'")
    x = GymPacmanEnvironment(layout_str=east2).reset()
    print(x)
    print(p_next(x, action))

    ## Problem 4
    print(f"Checking states space S_1 for k=1 in SS0tiny:")
    x = GymPacmanEnvironment(layout_str=SS0tiny).reset()
    states = get_future_states(x, N=10)
    for s in states[1]: # Print all elements in S_1.
        print(s)
    print("States at time k=10, |S_10| =", len(states[10]))

    ## Problem 6
    N = 20  # Planning horizon
    action, states = shortest_path(east, N)
    print("east: Optimal action sequence:", action)

    action, states = shortest_path(datadiscs, N)
    print("datadiscs: Optimal action sequence:", action)

    action, states = shortest_path(SS0tiny, N)
    print("SS0tiny: Optimal action sequence:", action)


def one_ghost():
    # Win probability when planning using a single ghost. Notice this tends to increase with planning depth
    wp = []
    for n in range(6):
        wp.append(win_probability(SS1tiny, N=n))
    print(win_probability(SS1tiny, N=12))

def two_ghosts():
    # Win probability when planning using two ghosts
    print(win_probability(SS2tiny, N=12))


###

from collections import defaultdict

def DP_stochastic(model):
    """
    Implement the stochastic DP algorithm. The implementation follows (Her21, Algorithm 1).
    In case you run into problems, I recommend following the hints in (Her21, Subsection 6.2.1) and focus on the
    case without a noise term; once it works, you can add the w-terms. When you don't loop over noise terms, just specify
    them as w = None in env.f and env.g.
    """
    N = model.N
    J = [{} for _ in range(N + 1)]
    pi = [{} for _ in range(N)]
    J[N] = {x: model.gN(x) for x in model.S(model.N)}
    for k in range(N-1, -1, -1):
        # All the states available at the time step k?
        for x in model.S(k):
            """
            Update pi[k][x] and Jstar[k][x] using the general DP algorithm given in (Her21, Algorithm 1).
            If you implement it using the pseudo-code, I recommend you define Q as a dictionary like the J-function such that
                        
            > Q[u] = Q_u (for all u in model.A(x,k))
            Then you find the u where Q_u is lowest, i.e. 
            > umin = arg_min_u Q[u]
            Then you can use this to update J[k][x] = Q_umin and pi[k][x] = umin.
            """
            # TODO: 4 lines missing.
            #print(k,x)
#            possible_returns = np.array([(u, sum([p_w * (model.g(x,u, w, k) + J[k+1][model.f(x, u, w, k)]) for w, p_w in model.Pw(x, u, k).items()])) for u in model.A(x, k)])
#            idx_best_return = argmin(possible_returns[:,1].astype(np.double))
#            pi[k][x], J[k][x] = (possible_returns[idx_best_return])
#            J[k][x] = float(J[k][x])
            Qu = {u: sum(pw * (model.g(x, u, w, k) + J[k + 1][model.f(x, u, w, k)]) for w, pw in model.Pw(x, u, k).items()) for u in model.A(x, k)} 
            umin = min(Qu, key=Qu.get)
            J[k][x] = Qu[umin]
            pi[k][x] = umin 

            #print(possible_returns)
            #print(pi[k][x], J[k][x] )
            
            #raise NotImplementedError("")
            """
            After the above update it should be the case that:

            J[k][x] = J_k(x)
            pi[k][x] = pi_k(x)
            """
    return J, pi



if __name__ == "__main__":

    # Create an environment with the given layout. animate_movement is just for a nicer visualization.
    
    # This creates a visualization (Note this makes the environment slower) which can help us see what Pacman does
    #env = VideoMonitor(env)
    # This create the GoAroundAgent-instance
    #agent = GoAroundAgent(env)


    #win_probability(SS1tiny, 6)
   # no_ghosts()
    one_ghost()
    #two_ghosts()
