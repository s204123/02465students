# This file may not be shared/redistributed without permission. Please read copyright notice in the git repo. If this file contains other copyright notices disregard this text.
import numpy as np
import sympy as sym
from scipy.optimize import Bounds
from gym.spaces import Box
import matplotlib.pyplot as plt
from irlc.ex04.continuous_time_discretized_model import DiscretizedModel
from irlc.ex04.continuous_time_environment import ContiniousTimeEnvironment
from irlc.ex04.continuous_time_model import ContiniousTimeSymbolicModel
from irlc.ex04.cost_continuous import SymbolicQRCost
from irlc.ex05.direct_agent import DirectAgent
from irlc.ex05.direct import get_opts
from irlc.ex06.dlqr import LQR
from irlc.ex06.linearization_agent import LinearizationAgent
from irlc.ex06.ilqr_agent import ILQRAgent
from irlc.project2.utils import render_car
from irlc import VideoMonitor, Agent, train, plot_trajectory, savepdf
from irlc.ex07.lqr_learning_agents import LearningLQRAgent, MPCLearningAgent, MPCLocalLearningLQRAgent

dt = 0.05 # Time discretization Delta
Tmax = 5  # Total simulation time (in all instances). This means that N = Tmax/dt = 100.
x22 = (2, 2, np.pi / 2)  # Where we want to drive to: x_target
#x22 = (0,2,0)

class R2D2Model(ContiniousTimeSymbolicModel): # This may help you get started.
    # TODO: 4 lines missing.
    #raise NotImplementedError("Define constants as needed here (look at other environments); Note there is an easy way to add labels!")
    action_space = Box(low=np.array([-np.inf, -np.inf]),high=np.array([np.inf, np.inf])) 
    observation_space = Box(low=np.array([-np.inf, -np.inf, - 2* np.pi]), high=np.array([np.inf, np.inf, 2 * np.pi]))
    state_labels = [r'$x$',r'$y$',r'$\gamma$']
    action_labels = [r'$v$',r'$\omega$']
    def __init__(self, x_target=(2,2,np.pi/2), Tmax=5., Q0=1.): # This constructor is one possible choice.
        simple_bounds = {
            'tF': Bounds([Tmax],[Tmax]),
            't0': Bounds([0],[0]),
            'x': Bounds([-np.inf, -np.inf, 0], [np.inf, np.inf, 2 * np.pi]),
            'u': Bounds([-np.inf, -np.inf],[np.inf, np.inf]),
            'x0': Bounds([0,0,0],[0,0,0]),
            'xF': Bounds(np.asarray(x_target),np.asarray(x_target))
        }  # Set this variable to correspond to the simple (linear) bounds the system is subject to. See exercises for examples.
        self.x_target = np.asarray(x_target)
        """ Since we have not discussed the cost-function a lot during the exercises it feels unreasonable to have you
        poke through the API. The following should do the trick: """
        cost = SymbolicQRCost(Q=np.zeros(self.state_size), R=np.eye(self.action_size))
        cost += cost.goal_seeking_cost(x_target=self.x_target)*Q0

        # TODO: 6 lines missing.
        #raise NotImplementedError("Complete model body.")
        # Set up a variable for rendering (optional) and call superclass.
        self.viewer = None
        super(R2D2Model, self).__init__(cost=cost, simple_bounds=simple_bounds)

    # TODO: 6 lines missing.
    #raise NotImplementedError("Complete model here.")

    """ These are two helper functions. They add rendering functionality so you can use the environment as
    > env = VideoMonitor(env) 
    and see a small animation. 
    """
    def close(self):
        if self.viewer is not None:
            self.viewer.close()

    def render(self, x, mode="human"): 
        return render_car(self, x, x_target=self.x_target, mode=mode) 
    
    def sym_f(self, x, u, t=None):
        
        return [u[0]*sym.cos(x[2]),u[0] * sym.sin(x[2]),u[1]]
    
    def reset(self):
        return np.zeros((3,))

class R2D2DiscreteModel(DiscretizedModel):
    def __init__(self, dt=0.05, Q0=0., x_target=x22, Tmax=5.):
        super().__init__(model=R2D2Model(x_target=x_target, Q0=Q0, Tmax=Tmax), dt=dt)

class R2D2Environment(ContiniousTimeEnvironment):
    def __init__(self, Tmax=Tmax, Q0=0., x_target=x22, dt=0.05):
        super().__init__(R2D2DiscreteModel(Q0=Q0, x_target=x_target, Tmax=Tmax, dt=dt), Tmax=Tmax)

# TODO: 9 lines missing.
#raise NotImplementedError("Your code here.")

def f_euler(x, u, Delta=0.05): 
    """ Solve Problem 13. The function should compute
    > x_next = f_k(x, u)
    """
    # TODO: 1 lines missing.
    x_next = x + Delta * np.array([u[0] * np.cos(x[2]), u[0] * np.sin(x[2]), u[1]])
    return x_next

def linearize(x_bar, u_bar, Delta=0.05): 
    """ Linearize R2D2's dynamics around the two vectors x_bar, u_bar
    and return A, B, d so that

    x_{k+1} = A x_k + B u_k + d (approximately)

    assuming that x_k and u_k are close to x_bar, u_bar. The function should return A, B and d.
    """
    # TODO: 2 lines missing.
    A = np.identity(3); A[[0,1],2] = [-u_bar[0] * np.sin(x_bar[2]) * Delta, u_bar[0] * np.cos(x_bar[2]) * Delta]
    B = np.zeros((3,2)); B[[0,1],0] = [np.cos(x_bar[2]) * Delta, np.sin(x_bar[2]) * Delta]; B[2,1] = Delta
    d = f_euler(x_bar,u_bar) - A @ x_bar - B @ u_bar
    return A, B, d


def drive_to_linearization(x_target, plot=True): 
    """
    Plan in a R2D2 model with specific value of x_target (in the cost function). We use Q0=1.0.

    this function will linearize the dynamics around xbar=0, ubar=0 to get a linear approximation of the model,
    and then use that to plan on a horizon of N=50 steps to get a control law (L_0, l_0). This is then applied
    to generate actions.

    Plot is an optional parameter to control plotting. the plot_trajectory(trajectory, env) method may be useful.

    The function should return the states visited as a (samples x state-dimensions) matrix, i.e. same format
    as the default output of trajectories when you use train(...).

    Hints:
        * The control method is identical to one we have seen in the exercises/notes. You can re-purpose the code from that week.
        * Remember to set Q0=1
    """
    A, B, d = linearize(np.zeros(3), np.zeros(2))
    N = 100
#    Q = np.zeros((3,3))
#    R = np.eye(2)
    model = R2D2DiscreteModel(dt=0.05,Q0=1,x_target=x_target,Tmax=5)
    env = R2D2Environment(5,1,x_target)
    Q, R, q, qc = model.cost.Q, model.cost.R, model.cost.q, model.cost.qc
    #(L,l), (V,v,vc) = LQR(A=[A]*N, B=[B]*N, d=[d]*N, Q=[Q]*N, R=[R]*N, q=[q]*N, qc=[qc]*N)
    (L,l), (V,v,vc) = LQR(A=[A]*N, B=[B]*N, d=[d]*N, Q=[Q]*N, R=[R]*N, q=[q]*N)
    u_bar = np.zeros((2)) 
    x_bar = np.zeros((3))
    u = []
    x = [x_bar]
    agent = LinearizationAgent(env, model=env.discrete_model, xbar=[0]*3, ubar=[0]*2)
    
    #agent = ILQRAgent(env, env.discrete_model, N=N)
    _, traj = train(env, agent, num_episodes=1, return_trajectory=True,)
    for k in range(N):
        #u.append( L[k] @ x[k] + l[k])
        u.append(agent.pi(x[k], k))
        x.append( A @ x[k] + B @ u[k])

    x = np.array(x)
    print(x)
    if plot:
        from irlc import plot_trajectory
        plot_trajectory(traj[0], env=model)
        #plt.plot(x[:,0], x[:,1], 'k-', label="R2D2's (x, y) trajectory")
        #plt.legend()
        #plt.xlabel("x")
        #plt.ylabel("y")
        #plot_trajectory(x, env=model)
    return traj[0].state 
    #return trajectories[0].state
    # TODO: 7 lines missing.
    #raise NotImplementedError("Implement function body")
    #return traj[0].state

def drive_to_direct(x_target, plot=False): 
    """
    Optimal planning in the R2D2 model with specific value of x_target using the direct method.
    Remember that for this problem we set Q0=0, and implement x_target as an end-point constraint (see examples from exercises).

    Plot is an optional parameter to control plotting, and to (optionally) visualize the environment using code such as

    if plot:
        env = VideoMonitor(env)

    For making the actual plot, the plot_trajectory(trajectory, env) method may be useful (see examples from exercises to see how labels can be specified)

    The function should return the states visited as a (samples x state-dimensions) matrix, i.e. same format
    as the default output of trajectories when you use train(...).

    Hints:
        * The control method (Direct method) is identical to what we did in the exercises, but you have to specify the options
        to implement the correct grid-refinement of N=10, N=20 and N=40.
        * The guess()-function will be automatically specified correctly assuming you implement the correct bounds. Use that function in the options.
    """
    # TODO: 11 lines missing.
    #model = R2D2Environment(5, 0, x_target)
    model = R2D2Model(x_target, 5, Q0=0)
    #d_model = DiscretizedModel(env=model)
    dmod = DiscretizedModel(model=model, dt=0.05)
    denv = ContiniousTimeEnvironment(discrete_model=dmod,Tmax=5)

    guess = model.guess()

    options = [
               get_opts(N=10, ftol=1e-07, guess=guess, verbose=False),
               get_opts(N=20, ftol=1e-07, guess=guess, verbose=False),
               get_opts(N=43, ftol=1e-07, guess=guess, verbose=False)
               ]
    
    env = R2D2Environment(5,0,x_target)
            
    agent = DirectAgent(env, guess=guess, options=options)
    stats, traj = train(env,agent,num_episodes=1, return_trajectory=True )

    if plot:
        from irlc import plot_trajectory
        plot_trajectory(traj[0], env=model)
#    raise NotImplementedError("Implement function body")
    return traj[0].state

class LINDeriv(Agent):

    def __init__(self, env, model, xbar=None, ubar=None):
        self.model = model
        N = 50  # Plan on this horizon. The control matrices will converge fairly quickly.
        """ Define A, B, d as the list of A/B matrices here. I.e. x[t+1] = A x[t] + B u[t] + d.
        You should use the function model.f to do this, which has build-in functionality to compute Jacobians which will be equal to A, B.
        It is important that you linearize around xbar, ubar. See (Her21, Section 15.1) for further details. """
        # TODO: 2 lines missing.
   
        #raise NotImplementedError("Compute control matrices L, l here using LQR(...)")
        self.env = env
        self.ubar = ubar
        #super().__init__(env)

    def pi(self,x, t=None):
        """
        Compute the action here using u_k = L_0 x_k + l_0. The control matrix/vector L_0 can be found as the output from LQR, i.e.
        L_0 = L[0] and l_0 = l[0].

        The reason we use L_0, l_0 (and not L_k, l_k) is because the LQR problem itself is an approximation of the true dynamics
        and this controller will be able to balance the pendulum for an infinite amount of time.
        """
        # TODO: 1 lines missing.
        #raise NotImplementedError("Compute current action here")
        instance = LinearizationAgent(self.env, self.model, x, self.ubar)
        self.ubar = instance.L[0] @ x + instance.l[0] 
        return self.ubar



    

def drive_to_mpc(x_target, plot=True): 
    """
    Plan in a R2D2 model with specific value of x_target (in the cost function) using iterative MPC (see text).
    In this problem, we set Q0=1.

    Plot is an optional parameter to control plotting. the plot_trajectory(trajectory, env) method may be useful.

    The function should return the states visited as a (samples x state-dimensions) matrix, i.e. same format
    as the default output of trajectories when you use train(...).

    Hints:
    * The control method is nearly identical to the linearization control method. Think about the differences,
    and how a solution to one can be used in another.
    """
    # TODO: 7 lines missing.
    #raise NotImplementedError("Implement function body")
    env = R2D2Environment(5,1,x_target)
    if False:
        ubar = [0]*2
        N = 100
        xbar=[0]*3
        x = [None] * (N+1)
        x[0] = np.zeros(3)
        u = [None] * (N+1)
        u[0] = np.zeros(2)
        u[N] = np.zeros(2)
        for k in range(N):

            agent = LinearizationAgent(env, model=env.discrete_model, xbar=x[k], ubar=u[k-1])

            _, traj = train(env, agent, num_episodes=1, return_trajectory=True,)
            u[k] = agent.L[0] @ x[k] + agent.l[0] 
            x[k+1] = traj[0].state[k+1]
            #ubar = traj[0].control[0]
        x = np.array(x)
        if plot:
            plt.plot(x[:,0], x[:,1], 'k-', label="R2D2's (x, y) trajectory")
            plt.legend()
            plt.xlabel("x")
            plt.ylabel("y")
        return x

    
    #agent = GOFCKYOURSELF(env,horizon_length=50,neighbourhood_size=30, u_bar=np.zeros(2),x_bar=np.zeros(3))
    agent = LINDeriv(env,env.discrete_model, None, [0]*2)
    _, traj = train(env, agent, num_episodes=1, return_trajectory=True,)

    if plot:
        from irlc import plot_trajectory
        plot_trajectory(traj[0], env=model)
        #plt.plot(x[:,0], x[:,1], 'k-', label="R2D2's (x, y) trajectory")
        #plt.legend()
        #plt.xlabel("x")
        #plt.ylabel("y")
        #plot_trajectory(x, env=model)
    return traj[0].state
    env = R2D2Environment(5,1,x_target)
    agent = MPCLearningAgent(env, horizon_length=50)
    stats, traj = train(env,agent,num_episodes=1, return_trajectory=True )

    if plot:
        from irlc import plot_trajectory
        plot_trajectory(traj[0], env=model)
#    raise NotImplementedError("Implement function body")
    return traj[0].state

if __name__ == "__main__":

    model = R2D2Model()

    # Check Problem 14
    x = np.asarray( [0, 0, 0] )
    u = np.asarray( [1,0])
    print("x_k =", x, "u_k =", u, "x_{k+1} =", f_euler(x, u, dt))

    A,B,d = linearize(x_bar=x, u_bar=u, Delta=dt)
    print("x_{k+1} ~ A x_k + B u_k + d")
    print("A:", A)
    print("B:", B)
    print("d:", d)

    # Test the simple linearization method (Problem 16)
    states = drive_to_direct(x22, plot=True)
    savepdf('r2d2_direct')
    plt.show()
#    # Build plot assuming that states is in the format (samples x coordinates-of-state).
    plt.plot(states[:,0], states[:,1], 'k-', label="R2D2's (x, y) trajectory")
    plt.legend()
    plt.xlabel("x")
    plt.ylabel("y")

    savepdf('r2d2_direct_B')
    plt.show()

    # Test the simple linearization method (Problem 17)
    drive_to_linearization((2,0,0), plot=True)
    savepdf('r2d2_linearization_1')
    plt.show()

    drive_to_linearization(x22, plot=True)
    savepdf('r2d2_linearization_2')
    plt.show()

    # Test iterative LQR (Problem 18)
    state = drive_to_mpc(x22, plot=True)
    print(state[-1])
    savepdf('r2d2_iterative_1')
    plt.show()
