# This file may not be shared/redistributed without permission. Please read copyright notice in the git repo. If this file contains other copyright notices disregard this text.
from ast import Del
import matplotlib.pyplot as plt
import numpy as np
from irlc import savepdf, train, plot_trajectory
from irlc.ex04.continuous_time_discretized_model import DiscretizedModel
from irlc.ex04.continuous_time_environment import ContiniousTimeEnvironment
from irlc.ex04.model_linear_quadratic import LinearQuadraticModel
from irlc.ex04.pid_locomotive_agent import PIDLocomotiveAgent
from irlc.ex06.lqr_agent import DiscreteLQRAgent

#extras
from irlc.ex04.locomotive import LocomotiveEnvironment
from irlc.ex06.dlqr import LQR

g = 9.82      # Gravitational force in m/s^2 
m = 0.1       # Mass of pendulum in kg
Tmax = 8      # Planning horizon in seconds
Delta = 0.04  # Time discretization constant 

# TODO: 24 lines missing.
# raise NotImplementedError("Your solution here")

class YODA_DisLQRAgent(DiscreteLQRAgent):
    def __init__(self, env, model):
        super().__init__(env, model)

    def pi(self, x, t=None):
        return super().pi(x, t=0)

from irlc.ex04.pid import PID
from irlc import Agent
class YodaPendulumPID(Agent):
    def __init__(self, env, v_target=0, Kp=1.5, Ki=0, Kd=0, dt=Delta):

        self.pid_velocity = PID(dt=dt, Kp=Kp, Ki=Ki, Kd=Kd, target=v_target)

        super().__init__(env)

    def pi(self, x, t=None):
        # x0 = angle
        # x1 = velocity
        u = np.asarray([self.pid_velocity.pi(x[0])])
        return u

class YodaPendulumModel(LinearQuadraticModel):
    def __init__(self, g=g, L=0.4, dt = Delta, m=m, Q=None, R=None, Tmax = Tmax):
        self.dt = dt
        self.A, self.B = get_A_B(g,L,m)

        # A = np.array([[0, 1],
        #             [1, 0]])
        # B = np.array([[0], [1]])

        if Q is None:
            Q = np.eye(2)*0.1
        if R is None:
            R = np.eye(1)*100

        super().__init__(A=self.A, B=self.B, Q=Q, R=R)

    def reset(self): # Return the initial state. In this case x = 1 and dx/dt = 0.
        return [1, 0]

class DisYodaPendulumModel(DiscretizedModel): 
    def __init__(self, dt=Delta, discretization_method=None, **kwargs):
        model = YodaPendulumModel(**kwargs)
        super().__init__(model=model, dt=dt, discretization_method=discretization_method)
        self.cost = model.cost.discretize(self, dt=dt) 

class YodaPendulumENV(ContiniousTimeEnvironment): 
    def __init__(self, Tmax=Tmax, supersample_trajectory=False, **kwargs):
        discrete_model = DisYodaPendulumModel(**kwargs)
        self.dt = discrete_model.dt
        super().__init__(discrete_model=discrete_model, Tmax=Tmax, supersample_trajectory=supersample_trajectory) 


def get_A_B(g, L, m=0.1): 
    """ Compute the two matrices A, B (see Problem 1) here and return them.
    The matrices should be numpy ndarrays. """

    # TODO: 2 lines missing.

    A = np.array([[0,1],[-g/L,0]])
    B = np.array([[0], [1/(m*L**2)]])

    return A, B

def cost_discrete(x_k, u_k): 
    """ Compute the (dicretized) cost at time k given x_k, u_k. in the Yoda-problem.
    I.e. the total cost is

    > Cost = sum_{k=0}^{N-1} c_k(x_k, u_k)

    and this function should return c_k(x_k, u_k).

    The idea behind asking you to implement this function is to test you have the right cost-function, as
    otherwise the code can be fairly hard to debug. If you are following the framework, you can implement the function
    using commands such as:

    dmodel = (create a discrete model instance here)
    return dmodel.c(x_k, u_k)

    If this worked, you will know you implemented the R, Q matrices correctly.
    """
    # TODO: 2 lines missing.

    Q = np.eye(2) * 0.1
    R = np.eye(1) * 100

    c_k = 1/2 * (x_k.T @ Q @ x_k + u_k.T @ R @ u_k)

    return c_k

def problem1(L): 
    """ This function solve Problem 2 by defining a PID controller and making the plot. The recommended way to do this
    is by implementing a very simple environment corresponding to Yodas pendulum (note we have several examples of linear models, including
    the Harmonic Osscilator, see model_harmonic.py), and then use an appropriate agent on it to simulate the PID controller.

    Hints:
        * You can perhaps be inspired by the locomotive-problem
        * The plot_trajectory(trajectory, environment) function is plenty fine for making this kind of plot. As an example:

    plot_trajectory(traj[0], env)
    plt.title("PID agent heuristic")
    savepdf("yoda1")
    plt.show()
    """
    # TODO: 7 lines missing.

    # env = LocomotiveEnvironment(m=m, slope=0, dt=Delta, Tmax=Tmax)
    # env.step()

    env = YodaPendulumENV(g=g, L=L, m=m)

    agent = YodaPendulumPID(env, v_target=0, Kp=0.4, Ki=5, Kd=0, dt=Delta)
    stats, traj = train(env, agent, return_trajectory=True) # Works with NullAgent(env) i think, Tror dog ikke den løser det atm. Tweak så den win condition er at den skal slutte med hastighed 0 (x1 =0)
    
    # kp = proportion of action. How much of the error is transferred into the action.
    # kd = derivative. intedned to fix overshooting and oscillations. The higher the less oscilations, This is added in the form of a kd * diff(e(t),t). handled by taking th prior point into account
    # ki = fixes droop by intergration over the error and adding this scaled to the action. As such if we keep being on one side, it will nudge us towards the middle. but if we alternate it wont do that much

    # The target is only directed a t the velocity, as the assignement is to stop the pendulum, but it does not specify that we cannot continously affect the pendulum. As such we can stop the pendulum before it reaches the bottom.
    # kp was originally set to 0.1, as this made it reach x1 = 0 in 1.5 seconds. But i really wanted to make it stop mid air. And as such i employed ki and tested som values, and found that if ki=5 and kp = 0.4 it stops very quickly. Using this configuration there are no ocilations, and as such kd = 0.

    plot_trajectory(traj[0], env)
    plt.title("PID agent heuristic")
    savepdf("yoda1")
    plt.show()
    # raise NotImplementedError("Implement function body")

def part1(L): 
    """ This function solve Problem 3.
    It should solve the Pendulum problem using an optimal LQ control law and return L_0, l_0 as well as the action-sequence
    obtained when controlling the system using this exact control law at all time instances k.

    Hints:
        * Although we don't have an agent that does *exact* what we want in the problem, we have one that comes *really* close.
    """
    # TODO: 3 lines missing.
    Q = np.eye(2) * 0.1
    R = np.eye(1) * 100

    env = YodaPendulumENV(g=g, L=L, m=m, Q=Q, R=R)
    agent = YODA_DisLQRAgent(env, model=env.discrete_model)
    stats, traj = train(env, agent, return_trajectory=True)

    # raise NotImplementedError("Return L0, l0, and the action sequence from the LQR controller")

    # plot_trajectory(traj[0], env)
    # plt.title("part 1")
    # plt.show()

    return agent.L[0], agent.l[0], traj[0].action

def part2(L): 
    """ This function should solve Problem 4. The function should return the action sequence
    obtained by treating the LQR control law as the parameters for a PID controller, and then simulating the system
    using that PID controller.

    The function should return

    > K_P, K_I, K_D, x_star, action_sequence

    in that order, where the first 4 terms specify a PID controller and the last is the corresponding action-sequence
    obtained by simulating the controller. The simulation can be done using the same method you used to
    simulate your pid controller.

    Hints:
        * Once you have specified the PID controller parameters, you can probably re-use what you did for Problem 1.

    """
    # TODO: 6 lines missing.
    Q = np.eye(2) * 0.1
    R = np.eye(1) * 100

    env = YodaPendulumENV(g=g, L=L, m=m, Q=Q, R=R)  # FACTS
    agent = YODA_DisLQRAgent(env, model=env.discrete_model)  # FACTS

    Kp = -np.array(agent.L)[0,0,0] # FACTS
    Kd = -np.array(agent.L)[0,0,1] # FACTS
    Ki = 0 # FACTS
    x_star = 0 # FACTS
    
    PID_agent = YodaPendulumPID(env, v_target=x_star, Kp=Kp, Ki=Ki, Kd=Kd, dt=Delta)

    stats, traj = train(env, PID_agent, return_trajectory=True)


    return Kp, Ki, Kd, x_star, traj[0].action

if __name__ == "__main__":
    L = 0.4  # Length of pendulum string; sorry that this clashes with (L_k, l_k)

    # Solve Problem 2
    problem1(L)

    # Optimal LQR to stop the pendulum Problem 3. Print control law and action sequence.
    L0, l0, u1 = part1(L)
    # print(L0, l0)
    # # Print the cost_term at time k, c_k(x_k, u_k), for a state/action:
    # print("Cost at time k is c_k(x_k, u_k) =", cost_discrete(np.asarray([-.1, 1]), np.asarray([-2])))

    # Solve Problem 4
    Kp, Ki, Kd, x_star, u2 = part2(L)
    # print(x_star, Kp, Kd, Ki)

    print(u2)

    # Plot the two action sequences (Problem 5)
    plt.plot(np.linspace(0, Tmax - Delta, len(u2)), u2, label="PID action sequence")
    plt.plot(np.linspace(0, Tmax - Delta, len(u1)), u1, label="LQR Optimal action sequence")
    plt.legend()
    savepdf("yoda1_actions")
    plt.show()
